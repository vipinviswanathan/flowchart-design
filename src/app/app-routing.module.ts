import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgxGraphModelComponent } from './ngx-graph-model/ngx-graph-model.component';
import { MxGraphModelComponent } from './mx-graph-model/mx-graph-model.component';


const routes: Routes = [
  {
    path: "",
    redirectTo: '/ngx-graph',
    pathMatch: 'full'
  },
  {
    path: 'ngx-graph',
    component: NgxGraphModelComponent
  },
  {
    path: "mx-graph",
    component: MxGraphModelComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
