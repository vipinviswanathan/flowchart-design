import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxGraphModelComponent } from './ngx-graph-model.component';

describe('NgxGraphModelComponent', () => {
  let component: NgxGraphModelComponent;
  let fixture: ComponentFixture<NgxGraphModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgxGraphModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxGraphModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
