import { Component, OnInit } from '@angular/core';
import * as shape from 'd3-shape';

@Component({
  selector: 'app-ngx-graph-model',
  templateUrl: './ngx-graph-model.component.html',
  styleUrls: ['./ngx-graph-model.component.scss']
})
export class NgxGraphModelComponent implements OnInit {
  ngOnInit(): void {
  }
  title = 'flow-chart';

  curves = [
    { name: 'linear', shape: shape.curveLinear },
    { name: 'basis', shape: shape.curveBasis },
    { name: 'curveCatmullRom', shape: shape.curveCatmullRom },
    { name: 'curveStep', shape: shape.curveStep },
    { name: 'curveStepAfter', shape: shape.curveStepAfter },
    { name: 'curveStepBefore', shape: shape.curveStepBefore },
    { name: 'natural', shape: shape.curveNatural },
  ];

  curve= this.curves[0].shape;

  layouts = ['dagre', 'dagreCluster', 'dagreNodesOnly', 'd3ForceDirected', 'colaForceDirected'];
  layout = this.layouts[4];

  links = [
    {
      id: 'a',
      source: 'first',
      target: 'second',
      label: 'is parent of'
    }, {
      id: 'b',
      source: 'first',
      target: 'third',
      label: 'custom label'
    },
    {
      id: 'c',
      source: 'first',
      target: 'fourth',
      label: 'is parent of'
    },
    {
      id: 'd',
      source: 'second',
      target: 'first',
      label: 'is parent of'
    },
    {
      id: 'e',
      source: 'second',
      target: 'first',
      label: 'repeat'
    }
  ];
  nodes = [
    {
      id: 'first',
      label: 'starting stage',
      color: 'green'
    }, {
      id: 'second',
      label: 'stage 1',
      color: 'orange'
    }, {
      id: 'third',
      label: 'stage 2',
      color: 'red'
    }, {
      id: 'fourth',
      label: 'stage 3',
      color: 'blue'
    }, {
      id: 'fifth',
      label: 'stage 4',
      color: 'pink'
    }
  ];
  clustures = [
    {
      id: 'third',
      label: 'Cluster node',
      childNodeIds: ['c1', 'c2']
    }
  ];

  clickLink(link) {
    console.log(link);
    alert("clicked : id => "+ link.id +" source => " + link.source + " target => " + link.target);
  }

  stageClick(node) {
    console.log(node);
    alert('clicked => '+ node.label + ' id => '+ node.id)
  }

}
