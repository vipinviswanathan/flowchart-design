import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-mx-graph-model',
  templateUrl: './mx-graph-model.component.html',
  styleUrls: ['./mx-graph-model.component.scss']
})
export class MxGraphModelComponent implements OnInit {

  ngOnInit(): void {

  }

  @ViewChild('graphContainer', {static: true}) graphContainer: ElementRef;

  ngAfterViewInit() {
    const graph = new mxGraph(this.graphContainer.nativeElement);

    try {
      const parent = graph.getDefaultParent();
      graph.getModel().beginUpdate();

      const vertex1 = graph.insertVertex(parent, '1', 'Vertex 1', 0, 0, 200, 80);
      const vertex2 = graph.insertVertex(parent, '2', 'Vertex 2', 0, 0, 200, 80);

      graph.insertEdge(parent, '', '', vertex1, vertex2);
      graph.insertEdge(parent, '', '', vertex2, vertex1);
      graph.insertEdge(parent, '', '', vertex2, vertex1);
      graph.insertEdge(parent, '', '', vertex2, vertex1);
      graph.insertEdge(parent, '', '', vertex2, vertex1);
      graph.insertEdge(parent, '', '', vertex2, vertex1);
    } finally {
      graph.getModel().endUpdate();
      new mxHierarchicalLayout(graph).execute(graph.getDefaultParent());
    }
  }

}
