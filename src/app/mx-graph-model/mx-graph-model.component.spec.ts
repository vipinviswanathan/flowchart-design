import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MxGraphModelComponent } from './mx-graph-model.component';

describe('MxGraphModelComponent', () => {
  let component: MxGraphModelComponent;
  let fixture: ComponentFixture<MxGraphModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MxGraphModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MxGraphModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
